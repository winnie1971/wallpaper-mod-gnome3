# wallpaper-mod-gnome

A Python script that scales or removes the wallpaper picture for the Gnome
desktop manager.

## How to

- Download `wallpaper-mod.py` then run:

      $ python3 wallpaper-mod.py
    
- Choose the desired option in the menu.

## Options
- none
- wallpaper
- centered
- scaled
- stretched
- zoom
- spanned
- reset

## License

GNU GPLv2+
