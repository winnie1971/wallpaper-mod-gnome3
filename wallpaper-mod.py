#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 21:03:12 2018

@author: Luciano Boccacciari
"""

# Operating system module import.
import os

picture_settings_command = 'gsettings set org.gnome.desktop.background picture-options'

# Each function correponds to a different setting.
def none():
    print('None')
    # String concatenation.
    modo2 = picture_settings_command + ' ' + '"none"'
    os.popen(modo2)
    print(modo2)

def wallpaper():
    print('Wallpaper')
    modo2 = picture_settings_command + ' ' + '"wallpaper"'
    os.popen(modo2)
    print(modo2)

def centered():
    print('Centered')
    modo2 = picture_settings_command + ' ' + '"centered"'
    os.popen(modo2)
    print(modo2)

def scaled():
    print('Scaled')
    modo2 = picture_settings_command + ' ' + '"scaled"'
    os.popen(modo2)
    print(modo2)

def stretched():
    print('Stretched')
    modo2 = picture_settings_command + ' ' + '"stretched"'
    os.popen(modo2)
    print(modo2)

def zoom():
    print('Zoom')
    modo2 = picture_settings_command + ' ' + '"zoom"'
    os.popen(modo2)
    print(modo2)

def spanned():
    print('Spanned')
    modo2 = picture_settings_command + ' ' + '"spanned"'
    os.popen(modo2)
    print(modo2)

def reset():
    print('Reset')
    os.popen('gsettings reset org.gnome.desktop.background picture-options')
    print('gsettings reset org.gnome.desktop.background picture-options')

# An empty function.
def get_out():
    pass

# A dictionary that enables to do the function selection from the menu.
tipo = { 0: none,
         1: wallpaper,
         2: centered,
         3: scaled,
         4: stretched,
         5: zoom,
         6: spanned,
         7: reset,
         8: get_out
}

# Print the menu.
print("0. none")
print("1. wallpaper")
print("2. centered")
print("3. scaled")
print("4. stretched")
print("5. zoom")
print("6. spanned")
print("7. reset")
print("8. exit")

done = False
while not done:
    # Transform the input string into an integer.
    scelta = input("Enter your choice: ")
    if scelta > '/' and scelta < '9':
        num=int(scelta)
        if num > -1 and num < 9:
            # Call the corresponding function.
            tipo[num]()
            done = True
        else:
            print("I do not have super-cow powers")
    else:
        print("I do not have super-cow powers")

print("All right, we're done")

